MapReduce Schedueling repo
--------------------------

We have literally no idea what we're doing.

**Mappakörnyezet:**

- **bigfile-test:** gyökérmappa
	- **file-generator:** egy kis C program, ami gyorsan tud irdatlan nagy fájlokat generálni
	- **mr_native:** MapReduce algoritmus (lenne) natív Javában (API-n keresztül)
	- **mr_streaming:** MapReduce algoritmus Javában, streaming funkcionalitáson keresztül
	- **mr_streaming-python:** MapReduce algoritmus Pythonban, streamingen keresztül
	- **tinyhadoop:** egy-gépes, lokális hadoop. Szimulálja a cluster környezetet, tesztelésre jó. Alapból git ignorálja, az `envsetup-script.sh` segítségével létrehozható.

A python, lévén nem igényel compilert, csak a source fájlokat tartalmazza, a többi esetben Sake-n keresztül épül a projekt, de az valójában csak egy bash scriptet / unix parancsot hív meg, tehát kikerülhető.

Java esetében a `javabuild.sh`.

C esetében egy kicsit bonyolultabb, így a `filegen` binary-t hozzáadtam a projekthez.

Sake: https://github.com/tonyfischetti/sake
