import java.util.*;


public class Reducer
{
	public static void main(String[] args)
	{
		long total = 0;
		int current = 0;
		String oldKey = new String();
		String thisKey = new String();
		
		Scanner scn = new Scanner(System.in);
		String buffer = new String();
		
		while (scn.hasNext())
		{
			buffer = scn.nextLine();
			String barray[] = buffer.split("\t");
			
			if (barray.length == 2)
			{
				// Type X	7462
				thisKey = barray[0];
				current = Integer.parseInt(barray[1]);
				
				if (oldKey.contains("Type") && (oldKey.compareTo(thisKey) != 0))
				{
					System.out.println(oldKey + "\t" + (total));
					
					total = 0;
					oldKey = thisKey;
				}
				
				oldKey = thisKey;
				total += current;
				
			}
			
		}
		scn.close();
		
		if (oldKey.contains("Type"))
		{
			System.out.println(oldKey + "\t" + total);
		}
		
	}
}
