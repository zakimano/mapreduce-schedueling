import java.util.*;


public class Mapper
{
	public static void main(String args[])
	{
		Scanner scn = new Scanner(System.in);
		String buffer = new String();
		
		while (scn.hasNext())
		{
			buffer = scn.nextLine();
			
			// RegEx: ([: a-zA-Z0-9\\t])+ || Type [A-Z]
			if (buffer.matches("([: a-zA-Z0-9\\t])+(Type [A-Z])\\tto\\t(Type [A-Z])([: a-zA-Z0-9\\t])+")) // skipping faulty lines
			{
				String barray[] = buffer.split("\t");
				// Transaction:	Type S	to	Type X	Amount:	7462
				System.out.println(barray[3] + "\t" + barray[5]);
			}
		}
		scn.close();
		
	}
}
