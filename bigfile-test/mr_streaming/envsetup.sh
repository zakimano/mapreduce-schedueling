
export JAVA_HOME=/usr/lib/jvm/default
export PATH=${JAVA_HOME}/bin:${PATH}
export HADOOP_CLASSPATH=${JAVA_HOME}/lib/tools.jar

alias hs="../tinyhadoop/hadoop-2.7.5/bin/hadoop jar ../tinyhadoop/hadoop-2.7.5/share/hadoop/tools/lib/hadoop-streaming-2.7.5.jar"

echo "Environment ready, use hadoop streaming via the 'hs' alias"
echo "usage: hs -mapper <Mapper> -reducer <Reducer> -input <input_folder> -output <output_folder>"
