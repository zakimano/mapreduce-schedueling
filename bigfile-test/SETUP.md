Env / Dir setup:
----------------

- file-generator
- mr_native
- mr_streaming
- mr_streaming-python
- tinyhadoop (not included in git repo)

Run envsetup-script.sh for automated setup of test env

- backup dl: https://www.apache.org/dyn/closer.cgi/hadoop/common/hadoop-2.7.5/hadoop-2.7.5.tar.gz
- hadoop home: https://hadoop.apache.org/index.html
- hadoop docs: https://hadoop.apache.org/docs/r2.7.5/
- hadoop mapreduce example: https://hadoop.apache.org/docs/r2.7.5/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html
