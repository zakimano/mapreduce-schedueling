#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#ifdef DEBUG
#include "debug_macros.h"
#endif
#include "headers/macros.h"
#include "headers/zc_argeval.h"
#include "headers/zc_auto_string.h"
#include "headers/line_generator.h"
#include "headers/file_printer.h"
#include "headers/loops.h"

#ifndef ITER_CHECK
#define ITER_CHECK 6000
#endif


// 3, 2, 1... BEGIN!


int main(int argc, char const *argv[])
{
	int rtval = 0;
	
	clock_t time_start = clock(); // measure how long this all takes
	
	struct arguments arguments;
	
	arguments.mode = 'g'; // 'g'
	arguments.file_size = 0; // 0
	arguments.output_file = "output.text"; // "output.text"
	
	arguments.buffer_size = 64; // 64
	arguments.tinies = 100; // 100
	arguments.tinies_size = 1000; // 1000
	arguments.smalls = 20; // 20
	arguments.smalls_size = 1; // 1
	
	#ifdef DEBUG_ARGS
	PRINT_DEBUG_INFO;
	#endif
	
	#pragma GCC diagnostic push // 3, 2, 1...
	#pragma GCC diagnostic ignored "-Wincompatible-pointer-types" // Stop this warning from appearing
	argp_parse(&argp, argc, argv, 0, 0, &arguments);
	#pragma GCC diagnostic pop // Let the warnings take over again.
	
	if (arguments.file_size <= 0 || arguments.buffer_size <= 0 || arguments.tinies <= 0 || arguments.tinies_size <= 0 || arguments.smalls <= 0 || arguments.smalls_size <= 0)
	{
		rtval = 1;
		printf("Please see --help or --usage.\n");
		return rtval;
	}
	
	
	#ifdef DEBUG_ARGS
	PRINT_DEBUG_INFO;
	#endif
	
	rand_init(); // Initialize rand. Guess we could save a timestamp? Not that it matters.
	
	// creating file pointer
	FILE *opf = NULL;
	opf = fopen(arguments.output_file, "w"); // trying to get lock on file
	if (opf == NULL) // if it wasn't successful
	{
		printf("Couldn't get lock on file %s\n", arguments.output_file);
		printf("\nSomething went really wrong. Please try again.\n");
		return 1;
	}
	
	
	astr buffer; // create and initialite the autostring buffer
	rtval = astr_init(&buffer);
	RTVAL_CHECK(rtval); // check if failed
	
	// MAIN FUNCTIONALITY (GENERATING)
	
	if (arguments.mode == 'l') // if we are only generating lines
	{
		rtval = loop_lines(&buffer, arguments.file_size); // generate dem lines to buffer
		RTVAL_CHECK(rtval); // we ok?
		
		rtval = print_buffer(&buffer, opf); // try printing it
		RTVAL_CHECK(rtval); // we still ok?
		
		astr_delete(&buffer); // then free memory ffs
		RTVAL_CHECK(rtval); // hooray.
	}
	else if (arguments.mode == 'g') // let's get down to business
	{
		int i, count;
		astr small_array[arguments.smalls]; // create and initialite the array for the smalls
		for (i = 0; i < arguments.smalls; i++)
		{
			rtval = astr_init(&small_array[i]); // init
			RTVAL_CHECK(rtval); // everything ok?
		}
		
		// create environment to build the buffer
		rtval = loop_bbuilder(arguments.tinies, arguments.tinies_size, arguments.smalls, arguments.smalls_size, small_array);
		RTVAL_CHECK(rtval); // we still okay?
		
		clock_t time_gen_start = clock(); // measure how long this takes
		
		// buid the buffer, print, then repeat until we reach the desired size.
		count = 0;
		while ((count * arguments.buffer_size) < (arguments.file_size * 1024)) // check if we've reached the size
		{
			rtval = loop_build_buffer(&buffer, arguments.buffer_size, arguments.smalls, small_array);
			RTVAL_CHECK(rtval); // hope we still ok
			
			rtval = print_buffer(&buffer, opf); // try printing it
			RTVAL_CHECK(rtval); // we still ok?
			
			count++;
		}
		
		clock_t time_gen_stop = clock();
		int msec = (time_gen_stop - time_gen_start) * 1000 / CLOCKS_PER_SEC;
		printf("Generating took: %ds %dms\n", msec/1000, msec%1000);
		
		for (i = 0; i < arguments.smalls; i++)
		{
			rtval = astr_delete(&small_array[i]); // init
			RTVAL_CHECK(rtval); // everything ok?
		}
		
		rtval = astr_delete(&buffer); // *sigh*
		RTVAL_CHECK(rtval);
	}
	else
	{
		printf("\nSomething went very wrong. Please try again.\n"); // lol how did you even manage this..?
		return 1;
	}
	
	// END OF GENERATING STUFF
	
	clock_t time_end = clock(); // measure how long this all takes
	
	int msec = (time_end - time_start) * 1000 / CLOCKS_PER_SEC;
	printf("Total time taken: %ds %dms\n", msec/1000, msec%1000);
	
	// Free memory (if needed)
	//rtval = astr_delete(&buffer); // *sigh*
	RTVAL_CHECK(rtval);
	
	return rtval;
}
