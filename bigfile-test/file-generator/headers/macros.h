#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>


#ifndef RTVAL_CHECK
#define RTVAL_CHECK(rtval) \
do \
{ \
	if (rtval) \
	{ \
		printf("\n!ERROR! Error code: %d\n", rtval); \
		return rtval; \
	} \
} while(0)
#endif

#ifndef SIZE_IN_MIB_CHECK
#define SIZE_IN_MIB_CHECK(buff) \
	((strlen(buff) * CHAR_BIT / 8) / (1024 * 1024))
#endif

#ifndef PRINT_DEBUG_INFO
#define PRINT_DEBUG_INFO \
do \
{ \
	printf("arguments mode: %c\n", arguments.mode); \
	printf("arguments file_size: %d\n", arguments.file_size); \
	printf("arguments output_file: %s\n\n", arguments.output_file); \
	printf("arguments buffer_size: %d\n", arguments.buffer_size); \
	printf("arguments tinies: %d\n", arguments.tinies); \
	printf("arguments tinies_size: %d\n", arguments.tinies_size); \
	printf("arguments smalls: %d\n", arguments.smalls); \
	printf("arguments smalls_size: %d\n\n\n", arguments.smalls_size); \
} while(0)
#endif
