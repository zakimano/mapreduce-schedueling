#pragma once

#include <stdlib.h>
#include <argp.h>
#include <stdbool.h>

/*
 * Header file for argument parsing, and general Argp stuff
 *
 * Docs:
 * https://www.gnu.org/software/libc/manual/html_node/Argp.html#Argp
 * https://stackoverflow.com/questions/9642732/parsing-command-line-arguments#24479532
 *
 * Maybe worth to look at:
 * https://github.com/jamesderlin/dropt
*/

#pragma GCC diagnostic push // 3, 2, 1...
#pragma GCC diagnostic ignored "-Wmissing-field-initializers" // Stop these warnings from appearing
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wincompatible-pointer-types"


// Define some basic info about the program
const char *argp_program_version = "filegen 0.2.3";
const char *argp_program_bug_address = "the GitLab issue tracker";
static char doc[] = "A tiny C program for generating huge files with pretty much random content.\nDo note that in default mode it first generates blocks, then starts building the buffer for the actual output file.";
static char args_doc[] = "[...]";

// Define help for options
static struct argp_option options[] = {
	{ "line-mode", 'l', 0, 0, "Switches from size in GiB to number of lines to generate. WARNING, in this case, the program will generate lines one-by-one, which can take a very long time for high numbers. Also, it will not take memory size into consideration, if you wish to generate 4 GiB worth of lines this way, you better have at least that much RAM (preferably much more)\n"},
	{ "output", 'o', "FILE", 0, "The output file, defaults to output.text\n"},
	{ "size", 's', "INT", 0, "The size of the file to generate, in GiB, required.\n"},
	{ "tinies", 10, "INT", 0, "Amount of tinies to generate (improves randomness, slows generation)\n"},
	{ "tinies-size", 11, "INT", 0, "Size of the tinies to generate (in lines)(the bigger, the faster it is to construct smalls as the next step)\n"},
	{ "smalls", 20, "INT", 0, "Amount of smalls to generate (improves randomness by a lot, slows generation by a lot)\n"},
	{ "smalls-size", 21, "INT", 0, "Size of the smalls to generate, defaults to 1 (in MiBs)(the bigger, the faster it is to construct the buffer)\n"},
	{ "buffer-size", 'b', "INT", 0, "The size of the buffer to use. (in MiBs) This controls how big chunks of data are written to file at a time. Defaults to 64\n"},
	{ 0 }
};

// Define the 'arguments' struct
struct arguments {
	char mode;
	int file_size, buffer_size, tinies, tinies_size, smalls, smalls_size;
	char * output_file;
};

// The actual parsing happens here.
// !! Make sure this aligns with the help section !!
static error_t parse_opt(int key, char *arg, struct argp_state *state) {
	struct arguments *arguments = state->input;
	switch (key)
	{
		case 'b': arguments->buffer_size = atoi(arg); break;
		case 'l': arguments->mode = 'l'; break;
		case 'o': arguments->output_file = arg; break;
		case 's': arguments->file_size = atoi(arg); break;
		case 10: arguments->tinies = atoi(arg); break;
		case 11: arguments->tinies_size = atoi(arg); break;
		case 20: arguments->smalls = atoi(arg); break;
		case 21: arguments->smalls_size = atoi(arg); break;
		case ARGP_KEY_ARG: return 0;
		default: return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc, 0, 0, 0 };

#pragma GCC diagnostic pop // Let warnings appear again

// Okay, we are done.

/* TO INCLUDE IN MAIN
int main(int argc, char *argv[])
{
	struct arguments arguments;
	
	arguments.mode = CHARACTER_MODE;
	arguments.isCaseInsensitive = false;
	
	argp_parse(&argp, argc, argv, 0, 0, &arguments);
	
	// ...
}
*/
