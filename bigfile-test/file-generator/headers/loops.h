#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>

#include "macros.h"
#include "zc_argeval.h"
#include "zc_auto_string.h"
#include "line_generator.h"
#include "file_printer.h"



int loop_lines(astr *buffer, int lines_to_gen) // just gen lines one-by-one
{
	int i, rtval = 0;
	
	for (i = 0; i < lines_to_gen; i++) // go until hit the desired amount
	{
		rtval = linegen(buffer); // calling linegen
		RTVAL_CHECK(rtval); // we ok?
	}
	
	return rtval;
}


int loop_build_buffer(astr *buffer, int buffer_size, int available_res, astr ares[])
{
	int rtval = 0;
	int pick = 0;
	int size_in_MiB = 0;
	
	while (size_in_MiB < buffer_size) // go until buffer's size reaches desired amount
	{
		pick = (rand() % available_res); // randomly pick an index from ares
		
		rtval = astr_append(buffer, ares[pick].start); // copy-paste the selected ares (aka tiny)
		RTVAL_CHECK(rtval); // we okay?
		
		size_in_MiB = SIZE_IN_MIB_CHECK(buffer->start); // update size counter
	}
	
	return rtval;
}


int loop_bbuilder(int tinies, int tinies_size, int smalls, int smalls_size, astr smalls_ret[])
{
	// int size_in_MiB = ((sizeof(buffer.start) * CHAR_BIT / 8) / (1024 * 1024));
	int rtval = 0;
	int i;
	
	astr tiny_array[tinies]; // create and initialite the array for the tiny ones
	for (i = 0; i < tinies; i++)
	{
		rtval = astr_init(&tiny_array[i]); // init
		RTVAL_CHECK(rtval); // everything ok?
	}
	
	for (i = 0; i < tinies; i++) // go through each tiny
	{
		// and generate tinies_size amount of lines into each of them
		rtval = loop_lines(&tiny_array[i], tinies_size);
		RTVAL_CHECK(rtval); // yay
	}
	
	for (i = 0; i < smalls; i++) // go through each of the smalls to generate
	{
		// go ahead and generate 'smalls_size' in MiBs sized 'smalls' (that go to smalls_ret[])
		// use the 'tinies' sized 'tiny_array[]' for this task
		rtval = loop_build_buffer(&smalls_ret[i], smalls_size, tinies, tiny_array);
		RTVAL_CHECK(rtval); // hope we still okay
	}
	
	for (i = 0; i < tinies; i++)
	{
		rtval = astr_delete(&tiny_array[i]); // init
		RTVAL_CHECK(rtval); // everything ok?
	}
	
	return rtval;
}
