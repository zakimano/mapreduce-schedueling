#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "zc_argeval.h"
#include "zc_auto_string.h"
#include "macros.h"


void rand_init()
{
	srand(time(NULL)); // d('-' )
}


int linegen(astr * buffer) // TODO should be configure-able via arguments or sample file. should also keep it backwards-compatible!
{
	int rtval = 0;
	
	long int rc = (rand() % 100);
	int r1 = (rand() % 26);
	int r2 = (rand() % 26);
	long int ra = (rand() % 100);
	
	if (rc)
	{
		char buff[64]; // Use an array which is large enough
		// !! snprintf, NOT sprintf !!
		snprintf(buff, sizeof(buff), "Transaction:\tType %c\tto\tType %c\tAmount:\t%ld\n", (65 + r1), (65 + r2), (ra * rc));
		
		rtval = astr_append(buffer, buff);
		RTVAL_CHECK(rtval);
	}
	else
	{
		rtval = astr_append(buffer, "--- DAMAGED LINE ---\n"); // lol rekt
		RTVAL_CHECK(rtval);
	}
	
	return rtval;
}
