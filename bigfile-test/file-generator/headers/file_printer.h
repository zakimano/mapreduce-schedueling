#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "zc_auto_string.h"
#include "macros.h"


int print_buffer(astr *buffer, FILE *opf)
{
	int rtval = 0;
	if (buffer == NULL || opf == NULL || buffer->start == NULL) // butae vagyol?
	{
		return 1; // há ne legyé'
	}
	
	fprintf(opf, buffer->start); // printing dem buffer, while hot
	
	rtval = astr_delete(buffer); // freeing memory
	RTVAL_CHECK(rtval); // are we still okay?
	rtval = astr_init(buffer); // re-initializing the buffer
	RTVAL_CHECK(rtval); // we better be still okay.
	
	return 0;
}
