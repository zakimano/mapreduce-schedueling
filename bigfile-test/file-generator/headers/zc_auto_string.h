#pragma once

#include <string.h>
#include <stdlib.h>


#ifndef ZC_ASTR_BASE
#define ZC_ASTR_BASE 64
#endif

#ifndef ZC_ASTR_MULT
#define ZC_ASTR_MULT 2
#endif


typedef struct auto_string
{
	char* start; // Always stays at the front
	char* end; // ALWAYS the string-ending zero
	int full_length; // Full length of allocated memory
} astr;


int astr_init(astr *toInit)
{
	char *newStr = (char *)calloc(ZC_ASTR_BASE, sizeof(char));
	if (newStr != NULL)
	{
		toInit->start = newStr;
		toInit->end = newStr + ZC_ASTR_BASE;
		toInit->full_length = ZC_ASTR_BASE;
	}
	else
	{
		return 1;
	}
	return 0;
}


int astr_delete(astr *toDelete)
{
	if (toDelete->start != NULL && toDelete->full_length != 0)
	{
		free(toDelete->start);
		toDelete->full_length = 0;
	}
	else if ((strlen(toDelete->start) == 0 && toDelete->full_length == 0) || (toDelete->start == NULL && toDelete->full_length == 0))
	{
		return 0;
	}
	else
	{
		printf("\nzc_auto_string: !ERROR!\n");
		return 1;
	}
	return 0;
}


int astr_append(astr *toAppendTo, char *strptr)
{
	if ((toAppendTo->full_length - strlen(toAppendTo->start)) > strlen(strptr))
	{
		strcat(toAppendTo->start, strptr);
	}
	else
	{
		#ifdef DEBUG_ZC_AUTO_STRING_H_
		int i = 0;
		#endif
		
		do
		{
			char* newStr = NULL; // Standard calloc here
			newStr = (char *)calloc(toAppendTo->full_length * ZC_ASTR_MULT, sizeof(char));
			if (newStr == NULL) // We good? We good.
				return 1;
			
			memcpy(newStr, toAppendTo->start, strlen(toAppendTo->start)); // Copy old string to new
			
			free(toAppendTo->start); // Free old string
			
			toAppendTo->start = newStr;
			if (toAppendTo->start == NULL) // hope it worked
				return 1;
			
			toAppendTo->full_length *= ZC_ASTR_MULT;
			
			#ifdef DEBUG_ZC_AUTO_STRING_H_
			printf("zc_auto_string: iter: %d, len: %d\n", i, toAppendTo->full_length);
			i++;
			#endif
			
			
		} while((toAppendTo->full_length - strlen(toAppendTo->start)) < strlen(strptr));
		
		strcat(toAppendTo->start, strptr);
	}
	
	return 0;
}
