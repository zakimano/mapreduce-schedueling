#!/bin/bash

echo ":: setting up environment ::"

mkdir tinyhadoop
cd tinyhadoop
wget "xenia.sote.hu/ftp/mirrors/www.apache.org/hadoop/common/hadoop-2.7.5/hadoop-2.7.5.tar.gz"
tar -xf hadoop-2.7.5.tar.gz
rm -rfv hadoop-2.7.5.tar.gz

echo ":: environment setup done ::"

exit
