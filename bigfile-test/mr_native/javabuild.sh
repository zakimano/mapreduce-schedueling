#!/bin/bash

export CLASSPATH=".:/home/zaki/Git-Projects/Zaki_on_GitLab/mapreduce-schedueling/bigfile-test/mr_native:${CLASSPATH}"

export JAVA_HOME=/usr/lib/jvm/default
export PATH=${JAVA_HOME}/bin:${PATH}
export HADOOP_CLASSPATH=${CLASSPATH}:${JAVA_HOME}/lib/tools.jar

#javac -g -deprecation *.java
echo ":: building $1 ::"
../tinyhadoop/hadoop-2.7.5/bin/hadoop com.sun.tools.javac.Main $1 #ezt megnézni
#javac -cp tinyhadoop/hadoop-2.7.5/bin/hadoop:. $1
#jar cf $1.jar *.class

exit
