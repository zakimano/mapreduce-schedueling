package mr_native;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.Mapper;


public class ZMapper extends Mapper<Object, Text, Text, IntWritable>
{
	public void map(Object key, Text val, Context context) throws IOException, InterruptedException
	{
		String buffer = new String(val.toString());
		
		Sajt a = new Sajt();
		
		// Process the <key, value> pair (assume this takes a while)
		
		// RegEx: ([: a-zA-Z0-9\\t])+ || Type [A-Z]
		if (buffer.matches("([: a-zA-Z0-9\\t])+(Type [A-Z])\\tto\\t(Type [A-Z])([: a-zA-Z0-9\\t])+")) // skipping faulty lines
		{
			String barray[] = buffer.split("\t");
			
			// Transaction:	Type S	to	Type X	Amount:	7462
			
			// Output the result
			Text b3 = new Text(barray[3]);
			IntWritable b5 = new IntWritable(Integer.parseInt(barray[5]));
			context.write(b3, b5);
		}
	}
}
