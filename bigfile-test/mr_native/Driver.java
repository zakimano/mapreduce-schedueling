package mr_native;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.*;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import mr_native.ZMapper;
import mr_native.ZReducer;


public class Driver
{
	/*
	public abstract class ZMapper extends Mapper<Object, Text, Text, IntWritable>
	{
		public void map(Object key, Text val, Context context) throws IOException, InterruptedException
		{
			String buffer = new String(val.toString());
			
			// Process the <key, value> pair (assume this takes a while)
			
			// RegEx: ([: a-zA-Z0-9\\t])+ || Type [A-Z]
			if (buffer.matches("([: a-zA-Z0-9\\t])+(Type [A-Z])\\tto\\t(Type [A-Z])([: a-zA-Z0-9\\t])+")) // skipping faulty lines
			{
				String barray[] = buffer.split("\t");
				
				// Transaction:	Type S	to	Type X	Amount:	7462
				
				// Output the result
				Text b3 = new Text(barray[3]);
				IntWritable b5 = new IntWritable(Integer.parseInt(barray[5]));
				context.write(b3, b5);
			}
		}
	}
	
	
	public abstract class ZReducer<Text> extends Reducer<Text, IntWritable, Text, IntWritable>
	{
		private IntWritable result = new IntWritable();
		
		public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException
		{
			int sum = 0;
			for (IntWritable val : values)
			{
				sum += val.get();
			}
			result.set(sum);
			context.write(key, result);
		}
	}
	*/
	
	public static void main(String[] args) throws Exception
	{
		// Init
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "MapReduceConfig");
		
		// No idea
		job.setJarByClass(Driver.class);
		
		// Set mapper, combiner (shuffle-sort?), reducer
		job.setMapperClass(ZMapper.class);
		job.setCombinerClass(ZReducer.class);
		job.setReducerClass(ZReducer.class);
		
		// Output classes
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		
		// IO Formats
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		
		// Done
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
