
export JAVA_HOME=/usr/lib/jvm/default
export PATH=${JAVA_HOME}/bin:${PATH}
export HADOOP_CLASSPATH=${JAVA_HOME}/lib/tools.jar

export CLASSPATH="/home/zaki/Git-Projects/Zaki_on_GitLab/mapreduce-schedueling/bigfile-test/mr_native:${CLASSPATH}"

alias hadoop="../tinyhadoop/hadoop-2.7.5/bin/hadoop"

echo "Environment ready, use hadoop via the 'hadoop' alias"
echo "usage: hadoop [jar <jar>] [...ARGS...]"
