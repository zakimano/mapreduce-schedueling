#!/usr/bin/python3

import sys
import fileinput
import re
import datetime
import platform


def main():
	
	# logging stuff
	with open("mapper.log", "a+") as logfile:
		
		i = j = 0
		name = platform.node()
		timestamp = datetime.datetime.now()
		logfile.write(str(timestamp) + "\t" + str(name) + "\t:: Mapper started ::\n")
		
		for line in sys.stdin:
			# more logs
			# timestamp = datetime.datetime.now()
			# logfile.write(str(timestamp) + "\t--in-:\t" + line.strip() + "\n")
			
			# RegEx: ([: a-zA-Z0-9])+ || \t || (Type [A-Z])
			if re.match("([: a-zA-Z0-9])+\t(Type [A-Z])\tto\t(Type [A-Z])\t([: a-zA-Z0-9])+\t([0-9])+", line):
				barray = line.split("\t")
				print(barray[3] + "\t" + barray[5].strip())
				
				# even more logs
				# timestamp = datetime.datetime.now()
				# logfile.write(str(timestamp) + "\t-out-:\t" + barray[3] + "\t" + barray[5].strip() + "\n")
				
				i = i + 1
				pass
			j = j + 1
			pass
		
		# End of log
		
		timestamp = datetime.datetime.now()
		logfile.write(str(timestamp) + "\t" + str(name) + "\t:: Mapper done ::\n"
		+ "\tValid lines: " + str(i) + "\tAll lines: " + str(j) + "\n")
		logfile.close()
		
		pass


main()
