#!usr/bin/python3

import sys
import fileinput


def main():
	total = 0
	current = 0
	oldKey = thisKey = ""
	
	for line in sys.stdin:
		barray = line.split("\t")
		if len(barray) == 2:
			thisKey = barray[0]
			current = int(barray[1])
			
			if ("Type" in oldKey) and (oldKey != thisKey):
				print(oldKey + "\t" + str(total))
				total = 0
				oldKey = thisKey
				pass
			
			oldKey = thisKey
			total += current
			pass
		
		pass
	
	if "Type" in oldKey:
		print(oldKey + "\t" + str(total))
		pass
	
	pass


main()
