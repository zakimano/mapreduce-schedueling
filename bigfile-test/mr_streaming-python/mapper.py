#!/usr/bin/python3

import sys
import fileinput
import re


def main():
	for line in sys.stdin:
		# RegEx: ([: a-zA-Z0-9])+ || \t || (Type [A-Z])
		if re.match("([: a-zA-Z0-9])+\t(Type [A-Z])\tto\t(Type [A-Z])\t([: a-zA-Z0-9])+\t([0-9])+", line):
			barray = line.split("\t")
			print(barray[3] + "\t" + barray[5].strip())
			pass
	pass


main()
